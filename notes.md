# Carousel con MEAN

Como quiero realizar un carousel o disponer de una serie de slides encadenados y sujetos a un servicio ```ÀPI-REST``` para un despliegue rápido con ```MEAN```;
en primer lugar, conceptualizo o realizo un pequeño esquema de las necesidades preparar el ambiente de trabajo. Como  se trata de utilizar el stack con ```MEAN``` he comprobado con que software, frameworks y paquetes voy a trabajar para instalar, leer, actualizar y desinstalar lo que necesito según el despliegue que requiere este stack ```(MongoDB, Express, Angular, NodeJS)``` para realizar un proyecto.

#### 1. Documentación

Compruebo que la documentación y cambios en el [log](http://meanjs.org/changelog.html) de [meanjs.org](http://meanjs.org/) y de [mean.io](http://mean.io/#!/) y preparo el espacio o ambiente de trabajo y los /directorios

#### 2. Ambiente de desarrollo

Al tratar con un stack completo que comprende varias tecnologías me parece importante tener actualizados  los gestores y servicios. La relación de tecnologías implicadas tiene una carga muy grande en todo el stack. Por tanto, actualizaciones de MongoDB, NodeJS, Angular o Express pueden significar la rotura de cohexiones anteriores.
######  Pruebas y el primer BUG
Algo que ya había discutido con anterioridad era la idoneidad de un paquete tan amplio, teniendo en cuenta las grandes ventajas que conlleva tener un stack tan dinámico y fácil de desplegar. Resulta que me encuentro con este error tan habitual:
*Anteriores stacks no levantan su servicio y su origen está en el driver  ``` Mongoose```, lo he actualizado y tampoco funciona* MongoDB y sobre todo Mongoose tienen un papel esencial, por tanto hay que partir de una versión nueva.

De hecho esto podía haberme pasado con  ```Node, Angular``` o ```Express``` . Hay que tener mucho cuidado en el ``` package.json``` y en las versiones del software disponible.

Por supuesto, este stack puede ser una buena solución si se controlan de forma independiente(y por tanto también en conjunto) el software principal y sus versiones, disponiendo de tu propio stack o dependes de que la empresa que libera el código [linnovate](http://www.linnovate.net/) promueve y mantenga actualizados los stacks, y puedas moverte libremente entorno a esta comunidad. Yo soy partidario de la primera opción, pero ambas son válidas si las circunstancias lo requieren.


```npm info``` y/o ```npm update```:
- El gestor de paquetes bower => ```$ npm update bower```
- MEAN trabaja con generadores y está muy ligado a a YEOMAN => ```$ npm update yo```
- GRUNT para automatizar las tareas habituales de despliegue, desarrollo y test ```$ npm update grunt```
	- Y limpio las cachés.

#### 3. Despliegue

Una vez situado en el directorio local desde el que voy desplegar la ap:
- Inicio el controlador de versiones: Git init
- Instalo el esqueleto
- Genero el módulo de Photo y establezco las entidades de CRUD.

#### 4. Carousel y flickr(problemas y conclusión)

Instalo el módulo de flickr [Angular-flickr](https://github.com/rustygreen/angular-flickr/) y aquí se que los siguientes pasos serían:

 - Según la página del [proyecto en Github](https://github.com/rustygreen/angular-flickr/) Podemos utilizar bower para instalarlo ``` bower install angular-flickr```
 - Tal y como nos indica, hemos de incluir los links a los archivos .css y .js externos en: ``` .../app/config/env/all.js``` con la referencia que figura en los estilos y js dados.
	 - ``` 'public/lib/angular-flickr/dist/angular-flickr.min.js'```
	 - ``` 'public/lib/angular-flickr/dist/angular-flickr.min.css'```
 - El siguiente paso es declarar el módulo como si fuera una dependencia más en: ```...//public/config.js```
y utilizar el módulo en nuestra vista ``` //public/modules/tumodulo/views/list-photo.client.view.html``` para obtener el resultado que queremos.

Es aquí donde necesito replantear el problema y buscar una solución desde 0 que tengo en módulo de pruebas. Se trata de instalar un servidor sencillo y práctico como [json-server](https://github.com/typicode/json-server) con el que podamos controlar los procesos y y las posteriores versiones de sus componentes para poder tener un control total sobre el stack.






